def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, repeats = 2)
  final_sentence = []
  repeats.times { final_sentence << string }
  final_sentence.join(" ")
end

def start_of_word(word, number_of_characters)
  word[0...number_of_characters]
end

def first_word(senetence)
  senetence.split(" ").first
end

def titleize(string)
  little_words = ["if","and","of","the","over","under","cause","when"]
  title = []
  string.split.each_with_index do |word, idx|
    if little_words.include?(word) && idx != 0
      title << word
    else
      title << word.capitalize
    end
  end
  title.join(" ")
end
