

def translate(sentence)
  sentence.split.map { |word| punctuation?(word) }.join(" ")
end

def pig_word(word)
  vowels = "aeiou"
  word = word.downcase.split("")
  until vowels.include?(word[0])
    word[0] == "q"? word << word.shift + word.shift : word << word.shift
  end
  word.join("") + "ay"
end

def capital?(word)
  word[0].upcase == word[0] ? pig_word(word).capitalize : pig_word(word)
end

def punctuation?(word)
  punc = ".!?,:;"
  has_punc = false
  if punc.include?(word[-1])
    stored_punc = word[-1]
    word = word[0..-2]
    has_punc = true
  end

  has_punc == true ? capital?(word) << stored_punc : capital?(word)

end
