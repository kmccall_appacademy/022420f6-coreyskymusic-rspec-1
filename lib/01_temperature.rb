def ftoc(temp)
  (temp - 32) * (5.0 / 9.0)
end

def ctof(temp)
  (temp.to_f / (5.0/ 9.0)) + 32
end
   
